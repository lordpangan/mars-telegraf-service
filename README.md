# MARS Telegraf Service

The telegraf service is deployed on all nomad clients, bound and listening to port 8125 which is the default port of statsd. The springboot application will send its metrics via statsd which then be scraped by the telegraf service and will be sent to influxdb.

### How do I get set up? ###

* The telegraf service is deployed on all nomad clients and bound to port 8125. All springboot applciation schedule on the nomad clietn will send its metrics to port 8125.
* To learn more on how to use the telegraf service check this [page](https://etsinternal.atlassian.net/wiki/spaces/TA/pages/2008154319/How+to+use+the+Telegraf+Image#Telegraf-Service).

### Building and pushing the image to the repository ###

```docker build -t etradingsoftware-marsfi-docker-local.jfrog.io/mars-telegraf-service:<version> .```

```docker push etradingsoftware-marsfi-docker-local.jfrog.io/mars-telegraf-service:<version>```

### Running the container on default mode ###
```docker run -e INFLUXDB_URL=localhost -e INFLUXDB_USERNAME=telegraf -e INFLUXDB_PASSWORD=password etradingsoftware-marsfi-docker-local.jfrog.io/mars-telegraf-service```

### Who do I talk to? ###

* DevOps Team