FROM telegraf:1.11.5
COPY telegraf.conf /etc/telegraf/telegraf.conf

# influxdb variable
ENV INFLUXDB_URL = "default.localhost"
ENV INFLUXDB_USERNAME = "telegraf"
ENV INFLUXDB_PASSWORD = "metricsmetricsmetrics"

# telegraf scraping interval
ENV FLUSH_INTERVAL = "10s"

ENTRYPOINT ["/entrypoint.sh"]
CMD ["telegraf"]